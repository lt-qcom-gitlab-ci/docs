# QCLT CI V2

Migrate lt-qcom jenkins pipelines into to CodeLinaro's gitlab CI. Work is tracked at Jira Epic [QLT-1806][qlt-1806]

## Jenkins to Gitlab migration

While Codelinaro is being prepared for large workloads to be executed in the cloud, pipeline migration currently
takes place at gitlab's group [lt-qcom-gitlab-ci][lt-qcom-gitlab-ci] and where [runners][runners] are hosted at
at `qcom-hackbox.linaro.org`.

## Gitlab CI Pipelines

The following table shows the pipelines migrated so far, poiting to the repository (which are located on top of
[lt-qcom-gitlab-ci][lt-qcom-gitlab-ci]), the trigger repository in case of scheduled pipelines and the trigger action

| Repository                        | Trigger repository | Trigger action|
| -----------                       | ----------- | ----------- |
| `bootloader/bootloader`           | `bootloader/trigger` | Scheduled polling on [db-boot-tools][db-boot-tools] repository |
| `linux-automerge/linux-automerge` | `linux-automerge/trigger` | Scheduled polling on [qcom configs][qcom-configs], [torvalds/linux][torvalds-linux] or [qualcomm automerge-rrcache][qualcomm-automerge-rrcache] repositories |
| `linux`                           | External qcom kernel repository | Kernel repository change |
| `oe/oe`                           | `oe/oe-rpb-manifest-poll` | Scheduled polling on [96boards/oe-rpb-manifest][oe-rpb-manifest] repository |
| `linux-testimages`                | `oe/oe-rpb-manifest-poll` | Scheduled polling on [96boards/oe-rpb-manifest][oe-rpb-manifest] repository |

## CodeLinaro Binary Artifacts

TODO: explain where the artifacts reside and create a table with repository and publish url columns

## LAVA testing

TODO: explain which repositories do LAVA testing

## Gitlab pipeline email status

TODO: show how gitlab reports back to users in case of build errors

## Gitlab to Codelinaro migration

TODO: explain what needs to be done in general terms, per repository

[qlt-1806]: https://linaro.atlassian.net/browse/QLT-1806
[lt-qcom-gitlab-ci]: https://gitlab.com/lt-qcom-gitlab-ci
[db-boot-tools]: https://git.linaro.org/landing-teams/working/qualcomm/db-boot-tools.git
[qualcomm-configs]: https://git.linaro.org/landing-teams/working/qualcomm/configs.git
[torvalds-linux]: git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
[qualcomm-automerge-rrcache]: https://git.linaro.org/landing-teams/working/qualcomm/automerge-rrcache.git
[landing-teams-working-qualcomm-kernel]: https://gitlab.com/lt-qcom-gitlab-ci/landing-teams/working/qualcomm/kernel
[codelinaro]: https://www.codelinaro.org/
[runners]: https://gitlab.com/groups/lt-qcom-gitlab-ci/-/runners
[oe-rpb-manifest]: https://github.com/96boards/oe-rpb-manifest.git
